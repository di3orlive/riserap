angular.module('starter', ['ionic', 'starter.controllers', 'starter.directives', 'ngAnimate', 'youtube-embed'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })






  .state('app.splash', {
    url: '/splash',
    views: {
      'menuContent': {
        templateUrl: 'templates/splash.html'
      }
    }
  })
  .state('app.location', {
    url: '/location',
    views: {
      'menuContent': {
        templateUrl: 'templates/location.html'
      }
    }
  })
  .state('app.rising', {
    url: '/rising',
    views: {
      'menuContent': {
        templateUrl: 'templates/rising.html',
        controller: 'risingCtrl'
      }
    }
  })
  .state('app.rapers', {
    url: '/rapers',
    views: {
      'menuContent': {
        templateUrl: 'templates/rapers.html',
        controller: 'rapersCtrl'
      }
    }
  })
  .state('app.favorites', {
    url: '/favorites',
    views: {
      'menuContent': {
        templateUrl: 'templates/favorites.html',
        controller: 'favoritesCtrl'
      }
    }
  })
  .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: 'settingsCtrl'
      }
    }
  })
  .state('app.invite', {
    url: '/invite',
    views: {
      'menuContent': {
        templateUrl: 'templates/invite.html',
        controller: 'inviteCtrl'
      }
    }
  })
  .state('app.confirm', {
    url: '/confirm',
    views: {
      'menuContent': {
        templateUrl: 'templates/confirm.html',
        controller: 'confirmCtrl'
      }
    }
  })
  .state('app.chooseBest', {
    url: '/chooseBest',
    views: {
      'menuContent': {
        templateUrl: 'templates/chooseBest.html',
        controller: 'chooseBestCtrl'
      }
    }
  })
  .state('app.chooseBestTags', {
    url: '/chooseBest/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/chooseBestTags.html',
        controller: 'chooseBestTagsCtrl'
      }
    }
  })
  .state('app.rise', {
    url: '/rise',
    views: {
      'menuContent': {
        templateUrl: 'templates/rise.html',
        controller: 'riseCtrl'
      }
    }
  });



  $urlRouterProvider.otherwise('/app/splash');
});






