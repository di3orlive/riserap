angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  $scope.searchOpen = false;

  $scope.toggleSearch = function(){
    $scope.searchOpen = !$scope.searchOpen;
  };






































  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('risingCtrl', function($scope, $http) {
  $scope.videos = [];

  $scope.playerVars = {
    rel: 0,
    showinfo: 0,
    theme: 'dark'
  };

  $scope.youtubeParams = {
    key: 'AIzaSyDNE9C_yoJbMGh_qqPZa5aFGPa-bOdiqqk',
    type: 'video',
    maxResults: '5',
    part: 'id,snippet',
    order: 'viewCount',
    channelId: 'UCDiPnpdOLNpEzvHzueGd7ZA'
  };

  $http.get('https://www.googleapis.com/youtube/v3/search', {params: $scope.youtubeParams}).success(function(response){
    angular.forEach(response.items, function(child){
      $scope.videos.push(child);
      console.log($scope.videos);
    });
  });
})

.controller('rapersCtrl', function($scope) {
})

.controller('favoritesCtrl', function($scope) {
  $scope.favoritesTab = 1;

  $scope.setTab = function(newTab){
    $scope.favoritesTab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.favoritesTab === tabNum;
  };
})

.controller('settingsCtrl', function($scope) {
})

.controller('inviteCtrl', function($scope) {
})

.controller('confirmCtrl', function($scope) {
})

.controller('chooseBestCtrl', function($scope) {
})

.controller('chooseBestTagsCtrl', function($scope, $stateParams) {
  $scope.id = $stateParams;
  console.log($stateParams);
})

.controller('riseCtrl', function($scope){
  $scope.riseTab = 1;

  $scope.setTab = function(newTab){
    $scope.riseTab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.riseTab === tabNum;
  };
});








